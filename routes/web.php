<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//admin Registration

Route::get('owner/reg','OwnerController@Registration');
Route::post('owner/reg','OwnerController@NewRegistration');
//admin_owner_panel
Route::get('owner/home','OwnerController@Home');
Route::get('owner/login','UserLogin@Index');
Route::post('owner/home','OwnerController@CheckLogin');
Route::get('owner/logout','OwnerController@logout');


Route::post('owner/personal-info','OwnerController@personalInformation');
Route::post('owner/building-information','OwnerController@buildingInformation');
Route::post('owner/flat-information','OwnerController@flatInformation');
Route::post('owner/man-power','OwnerController@manPower');


//admin_manage_panel
Route::get('owner/all-properties','ManageController@allProperty');
Route::get('owner/add-more','ManageController@addMoreProperty');
//Route::post('owner/more-man-power','ManageController@MoremanPower');


//add information for more property
Route::post('owner/more-building-information','ManageController@MorebuildingInformation');
Route::post('owner/add-more-flat','ManageController@addMoreflat');
Route::post('owner/add-more-man','ManageController@addMoreman');


//edit own property....with widget
Route::get('owner/edit-widget/{reference_id}/{owner_id}','ManageController@EditMoreProperty');

//Manage home and edit data
Route::get('owner/management/{reference_id}/{owner_id}','ManageController@ManageProperty');
Route::get('owner/edit-property/{reference_id}/{owner_id}','ManageController@EditManageProperty');




//manage_renter
Route::get('owner/add_renter/{reference_id}/{owner_id}','OwnertoRenterManagement@AddRenterManageProperty');
Route::get('owner/show_floor/{floor}/{reference_id}/','OwnertoRenterManagement@ShowFloorDatajs');
Route::post('owner/save_renter_data','OwnertoRenterManagement@SaveRenterData');

Route::get('owner/manage_renter/{reference_id}/{owner_id}','OwnertoRenterManagement@EditRenterManageProperty');
Route::get('owner/data-handling/{id}/{floor}/{flat}/{reference}','OwnertoRenterManagement@DataHandling');




Route::POST('owner/renter-dashboard','OwnertoRenterManagement@RenterDashboard');











Route::get('owner/add/','ManageController@AddProperty');
Route::post('owner/add','ManageController@SaveProperty');
Route::post('owner/delete','ManageController@DeleteProperty');


Route::get('owner/edit','ManageController@EditProperty');
Route::post('owner/edit','ManageController@SaveEditedProperty');

Route::post('owner/history','ManageController@OwnerHistory');
Route::get('owner/history','ManageController@PropertyHistory');




Route::get('owner/rentcost/{id}/{uid}','ManageController@CreateRentCost');
Route::post('owner/createcost/','ManageController@SendCreateRentCost');
Route::post('owner/rentcost/','ManageController@SavePaidCost');


Route::get('owner/total-history','ManageController@BillHistory');



//renter_owner_panel
Route::get('renter/myhome','RenterController@Index');
Route::post('renter/myhome','RenterController@CheckRenterLogin');
Route::get('renter/renterlogin','RenterLogin@Index');
Route::get('renter/renterlogout','RenterLogin@RenterLogout');

//Renter Manage Pannel
Route::get('renter/history','RenterManageController@Index');
Route::post('renter/history','RenterManageController@AcceptPayment');