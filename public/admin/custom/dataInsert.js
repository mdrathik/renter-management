$(document).ready(function () {
    var i = 0;
    $("#flatcount").blur(function () {
        var floor = (this).value;
        $('#row').empty();
        for (i = 1; i <= floor; i++) {
            $('#row').append('<input  type="text" name="flat[]" id="' + i + '" class="form-control" placeholder="Flat  ' + i + ' Name"><br>');

        }
    });
});


//manPower Data
$(document).ready(function () {
    var max_fields = 10; //maximum input boxes allowed
    var wrapper = $(".input_fields_wrap"); //Fields wrapper
    var add_button = $(".add_field_button"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function (e) { //on add input button click
        e.preventDefault();
        if (x < max_fields) { //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div style="display: flex"><input required name="post[]" type="text" class="form-control" placeholder="ex: Manager/Post"> &nbsp;&nbsp; <input required name="salary[]" type="number" class="form-control" placeholder="ex: 3000 BDT"> &nbsp;&nbsp;<input required type="text" name="name[]" type="number" class="form-control" placeholder="ex: Md Example uddin"> &nbsp;&nbsp;<a href="#" class="remove_field btn btn-danger"><span class="glyphicon glyphicon-minus-sign"></span></a></div><br>'); //add input box
        }
    });

    $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
        e.preventDefault();
        $(this).parent('div').remove();
        x--;
    })
});


$(document).on('click', '#ownerData', function (e) {
    var data = $('#ownerForm').serialize();
    //alert(data);
    $.ajax({
        url: 'add',
        method: 'POST',
        data: data,

        success: function (data) {
            alert(data);
            if (data == 'success') {
                $('#inputmsg').hide();
                $('#alert').addClass("alert alert-success").text('Saved Successfully').fadeIn().fadeOut(2000);
                location.reload();
            }

        }
    });
});



$(document).ready(function (e) {
    $('#floor_check').on('change',function () {
        var floor_check=(this).value;
        var reference_id=$('#reference_id').val();
        //alert(reference_id);

        if(floor_check){
        $.ajax({
            url: '/house_rent/owner/show_floor/'+floor_check+'/'+reference_id,
            method: 'GET',
            success:function (data) {
                //alert(data);
                $('#flat').html(data);
            }
        })}

        else{
                $('#flat').html('<option value="">Select floor first</option>');
        }

    })

});




$(document).ready(function(){
    $('.filterable .btn-filter').click(function(){
        var $panel = $(this).parents('.filterable'),
            $filters = $panel.find('.filters input'),
            $tbody = $panel.find('.table tbody');
        if ($filters.prop('disabled') == true) {
            $filters.prop('disabled', false);
            $filters.first().focus();
        } else {
            $filters.val('').prop('disabled', true);
            $tbody.find('.no-result').remove();
            $tbody.find('tr').show();
        }
    });

    $('.filterable .filters input').keyup(function(e){
        /* Ignore tab key */
        var code = e.keyCode || e.which;
        if (code == '9') return;
        /* Useful DOM data and selectors */
        var $input = $(this),
            inputContent = $input.val().toLowerCase(),
            $panel = $input.parents('.filterable'),
            column = $panel.find('.filters th').index($input.parents('th')),
            $table = $panel.find('.table'),
            $rows = $table.find('tbody tr');
        /* Dirtiest filter function ever ;) */
        var $filteredRows = $rows.filter(function(){
            var value = $(this).find('td').eq(column).text().toLowerCase();
            return value.indexOf(inputContent) === -1;
        });
        /* Clean previous no-result if exist */
        $table.find('tbody .no-result').remove();
        /* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */
        $rows.show();
        $filteredRows.hide();
        /* Prepend no-result row if all rows are filtered */
        if ($filteredRows.length === $rows.length) {
            $table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="'+ $table.find('.filters th').length +'">No result found</td></tr>'));
        }
    });
});