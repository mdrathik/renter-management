@extends('layouts.master')
@section('content')
    <script src="{{asset('public/admin/')}}/vendor/jquery/jquery.js"></script>
    <div class="main-content">
        <div class="container-fluid">
            <div class="form-group pull-right">
                <input type="text" class="search form-control" placeholder="What you looking for?">
            </div>
            <span class="counter pull-right"></span>
            <table class="table table-hover table-bordered results">
                <thead>
                <tr>
                    <th>Invoice</th>
                    <th>Name</th>
                    <th>floor/flat</th>
                    <th>House</th>
                    <th>Gas</th>
                    <th>Water</th>
                    <th>Current</th>
                    <th>Maint.</th>
                    <th>Extra</th>
                    <th>Total</th>
                    <th>Date</th>
                    <th>Comment</th>


                </tr>
                <tr class="warning no-result">
                    <td colspan="12"><i class="fa fa-warning"></i> No result</td>
                </tr>
                </thead>
                <tbody>
             @foreach($all_data as $key)

                <tr>
                    <td>{{$key->invoice_id}}</td>
                    <th>{{$key->username}}</th>
                    <th>{{$key->floor}}/
                        @if($key->flat==1)Left Side
                        @elseif($key->flat==2)Middle Side
                        @elseif($key->flat==3)Right Side
                        @endif
                    </th>
                    <td>{{$key->house_rent}}</td>
                    <td>{{$key->gas_bill}}</td>
                    <td>{{$key->water_bill}}</td>
                    <td>{{$key->current_bill}}</td>
                    <td>{{$key->maintainance_charge}}</td>
                    <td>{{$key->extra_charge}}</td>
                    <td>{{$key->house_rent+$key->current_bill+$key->gas_bill+$key->water_bill+$key->maintainance_charge+$key->generator_bill+$key->extra_charge}} BDT</td>
                    <td>{{$key->created_at}}</td>
                    <td>{{$key->comment}}</td>
                </tr>

                @endforeach

                </tbody>
            </table>


    </div>
    </div>


<script>
    $(document).ready(function() {
        $(".search").keyup(function () {
            var searchTerm = $(".search").val();
            var listItem = $('.results tbody').children('tr');
            var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

            $.extend($.expr[':'], {'containsi': function(elem, i, match, array){
                return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
            }
            });

            $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
                $(this).attr('visible','false');
            });

            $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
                $(this).attr('visible','true');
            });

            var jobCount = $('.results tbody tr[visible="true"]').length;
            $('.counter').text(jobCount + ' item');

            if(jobCount == '0') {$('.no-result').show();}
            else {$('.no-result').hide();}
        });
    });

</script>

@endsection
