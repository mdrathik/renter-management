@extends('layouts.master')
@section('content')
    <div class="main-content">
        <div class="container-fluid ">
            <h1 class="section-title">Manage Your Property</h1>
            <div class="title-underline"></div>


            <div class="row col-md-offset-1">
                        <div class="col-lg-12 ">

                            <p>
                                <a title=" Manage Your Renter" href="{{url('owner/add_renter')."/".$reference_id."/".$owner_id}}" class="btn btn-sq-lg btn-success">
                                    <i class="fa fa-plus-circle fa-5x"></i><br/>
                                   Add Your Renter
                                </a>

                                <a href="{{url('owner/manage_renter')."/".$reference_id."/".$owner_id}}" title="Billing History" class="btn btn-sq-lg btn-warning">
                                    <i class="fa fa-magnet fa-5x"></i><br/>
                                    Manage Renter
                                </a>
                                <a href="#" title="Billing History" class="btn btn-sq-lg btn-info">
                                    <i class="fa fa-usd fa-5x"></i><br/>
                                    Billing History
                                </a>
                                <a href={{url('owner/edit-property')."/".$reference_id."/".$owner_id}} title="Edit" class="btn btn-sq-lg btn-primary">
                                    <i class="manage_icon fa fa-edit fa-5x"></i><br/>
                                    Edit
                                </a>


                                <a href="#" title="Delete" class="btn btn-sq-lg btn-danger">
                                    <i class="fa fa-trash fa-5x"></i><br/>
                                    Delete
                                </a>
                            </p>
                        </div>
                    </div>
    </div>
    </div>

@endsection