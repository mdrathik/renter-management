<script src="{{asset('public/admin/vendor/jquery/jquery.js')}}"></script>
<script src="{{asset('public/admin/custom/dataInsert.js')}}"></script>
@extends('layouts.master')
@section('content')
    <div class="main-content">
        <h1 class="section-title">Your all  Properties</h1>
        <a href="{{url('owner/add-more/')}}" title="Add New Home"  class="btn navbar-btn-right btn-success btn-circle btn-lg"><span class="glyphicon glyphicon-plus"></span></a>
        <div class="title-underline"></div>
        <div class="container-fluid">
            <div class="row">

                        <div class="promo-flex ">
                            @foreach($all_property as $key)
                                <?php $r_id=$key->reference_id;
                                      $o_id=$key->owner_id;
                                      $owner_step=DB::table('owner_step')->where('owner_id',$o_id)->where('reference_id',$r_id)->first();
                                ?>
                                 @if($owner_step->step_id==5)
                                <a href="{{url('owner/management')."/".encrypt($key->reference_id)."/".$key->owner_id}}">
                                @else
                                <a href="{{url('owner/edit-widget')."/".$key->reference_id."/".$key->owner_id}}">
                                  {{-- {{Session::put('message','You have a incomplete Property. Please submit your all data')}}--}}
                                    @endif
                            <div  data-ix="blog-card" class="w-clearfix w-preserve-3d promo-card">
                               <img class="center-block" width="150px" height="150px" src="{{asset('public/admin/')}}/img/home.png">
                                @if($owner_step->step_id==5)
                                <div  class="blog-bar color-blue"></div>

                                @else
                                    <div  class="blog-bar color-pink"></div>
                                @endif
                                <div class="blog-post-text">
                                    {{$key->building_name}}
                                    <div class="blog-description blue-text">{{$key->building_address}}</div>
                                    <div class="blog-description blue-text">ref#{{$key->reference_id}}</div>
                                </div>
                            </div>
                                </a>

                            @endforeach
                                </a>


                        </div>


                        </div>
                          </div>

                    </div>
                </div>
            </div>


@endsection
<script>
    $(window).load(function() {
        $('.post-module').hover(function() {
            $(this).find('.description').stop().animate({
                height: "toggle",
                opacity: "toggle"
            }, 300);
        });
    });
</script>
<script>
    window.setTimeout(function() {
        $(".alert").fadeTo(400, 0).slideUp(500, function(){
            $(this).remove();
        });
    }, 5000);
</script>
