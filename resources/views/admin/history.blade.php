@extends('layouts.master')
@section('content')

    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="text-center">{{Session::get('msg')}}</h3>
                    <div class="fancy-collapse-panel">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <?php  $property = DB::table('property_details')->where('name',Session::get('name'))->get() ?>
                            @foreach($property as $key)
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingTwo">

                                        <h4 class="panel-title">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#{{$key->reference_id}}" aria-expanded="false" aria-controls="collapseTwo">
                                                Reference_ID-- {{$key->reference_id}}  ||  {{$key->address}}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="{{$key->reference_id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="panel-body">

                                            <!-- TABLE NO PADDING -->
                                            <div class="panel">
                                                <div class="panel-body no-padding">
                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Full name</th>
                                                            <th>Username</th>
                                                            <th>Floor</th>
                                                            <th>flat</th>
                                                            <th>email</th>
                                                            <th>Mobile</th>
                                                            <th>Action</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php  $rent = DB::table('flat_details')->where('reference_id',$key->reference_id)->get();
                                                        foreach($rent as $key2=>$value){ ?>
                                                        <tr>
                                                            <td>{{$value->id}}</td>
                                                            <td>{{$value->user_fullname}}</td>
                                                            <td>{{$value->username}}</td>
                                                            <td>{{$value->floor}}</td>
                                                            @if($value->flat=="1")
                                                            <td>Left</td>
                                                            @elseif($value->flat=="2")
                                                                <td>Middle</td>
                                                            @else
                                                                <td>Right</td>
                                                            @endif
                                                            <td>{{$value->email}}</td>
                                                            <th>{{$value->mobile}}</th>
                                                            <th><a title="Create Cost" href="rentcost/{{$key->reference_id}}/{{$value->username}}" class="btn-xs btn-warning"><span class="glyphicon glyphicon-usd"></span></a>
                                                                </th>
                                                        </tr>
                                                        <?php }
                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <!-- END TABLE NO PADDING -->

                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>

@endsection
