<script src="{{asset('public/admin/vendor/jquery/jquery.js')}}"></script>
<script src="{{asset('public/admin/custom/dataInsert.js')}}"></script>
<script src="{{asset('public/admin/custom/random.js')}}"></script>
@extends('layouts.master')
@section('content')

    <h1 class="section-title">Add your Renter - <strong
                style="color: forestgreen">{{$property_details->building_name}}</strong></h1>
    <h4 class="text-center">{{$property_details->building_address}} #{{$property_details->reference_id}}</h4>
    <div class="title-underline"></div>
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">


                <form method="post" action={{url('owner/save_renter_data')}} class="form-horizontal">
                    <input type="hidden" id="reference_id" name="reference_id" value="{{$property_details->reference_id}}">
                    <input type="hidden" name="owner_id" value="{{$property_details->owner_id}}">
                    <input type="hidden" name="renter_reference" value="{{rand(10000,99999)}}">


                    {{csrf_field()}}

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="store">Renter Role</label>
                        <div class="col-md-4">
                            <select id="test" required name="role" class="form-control">
                                <option disabled="disabled" selected="selected">Select Renter Role</option>
                                <option value="renter">Renter</option>
                                <option value="developer">Developers</option>
                                <option value="3rdparty">3rd Party</option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-4 control-label" for="store">Select Your Choose</label>
                        <div class="col-md-4">
                            <select id="test" required onchange="showDiv(this)" name="type"
                                    class="form-control">
                                <option disabled="disabled" selected="selected">Select Type</option>
                                <option value="single">Single Flat with Single Floor</option>
                                <option value="multiple">Multiple Flat With Multiple Floor</option>

                            </select>
                        </div>
                    </div>


                    <fieldset>

                        <div style="display: none"   id="multiple">
                            <label class="col-md-4 control-label col-lg-4" for="store">Select Your floor and
                                Flat</label>

                            <div class="form-group col-md-8 ">
                                <?php $a=0; ?>
                                @for($i=1;$i<=$property_details->floor;$i++)
                                    <?php
                                        $a++;
                                    $newflat = DB::table('floorbyflat')->where('reference_id',$property_details->reference_id)
                                        ->where('floor',$i)
                                        ->get();

                                       $floorbyflat = explode("~", $newflat[0]->flat);

                                       /* <?php
                                                $available = DB::table('renter_property_details')
                                                    ->where('reference_id',$property_details->reference_id)
                                                    ->where('floor',1)->first();
                                                $stdClass = json_decode(json_encode($available),true);
                                                $available = explode("~", $stdClass["flat"]);
                                                ?>*/

                                    ?>


                                        <div class="columns">
                                            {{$i}} floor &nbsp;&nbsp;
                                            <input hidden  type="number" value="{{$i}}" name="floor[]">
                                            @foreach($floorbyflat as $key1)
                                                        <label id="Checkboxes_Apple-{{$i}}{{$key1}}xx" class="checkbox-inline" for="Checkboxes_Apple-{{$i}}{{$key1}}">
                                                            <input  class="balsal{{$i}}"  type="checkbox" about="{{$i}}" name="flat[{{$i}}][]" id="Checkboxes_Apple-{{$i}}{{$key1}}" value="{{$key1}}">
                                                            {{$key1}}
                                                        </label>
                                            @endforeach

                                        </div>
                                @endfor

                                    <input hidden  type="number" value="{{$i}}" id="floorcount">
                            </div>


                            {{--<div id="single">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="store">Select Your Floor</label>
                                <div class="col-md-4">
                                    <input type="hidden" id="reference_id" name="reference_id"
                                           value="{{$property_details->reference_id}}">
                                    <select id="floor_check" id="floor" name="floor" class="form-control">
                                        <option disabled="disabled" selected="selected" value="#">Select Your Floor</option>
                                        @for($i=1;$i<=$property_details->floor;$i++)
                                            <option value="{{$i}}">{{$i}} floor</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>--}}

                            {{--<div class="form-group">
                                <label class="col-md-4 control-label" for="store">Select Your Flat </label>
                                <div class="col-md-4">
                                    <select id="flat" name="flat" class="form-control">
                                        <option value="Store 1">Please Select Floor First</option>
                                    </select>
                                </div>
                            </div>--}}

                        </div>


                        <!-- Appended checkbox -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="giftPic">Renter Username and Password</label>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <input id="username" name="username" class="form-control" type="text"
                                           placeholder="Username">

                                    <input id="password" name="password" class="form-control" type="text"
                                           placeholder="Password">

                                    <span style="background: #55acee;" id="checkArray" class="input-group-addon">
                                        <span style="color: white" class="help-block glyphicon glyphicon-random"> Generate Random</span>
                                      <input id="ramdom_gen" name="ramdom_gen" type="checkbox">
                                 </span>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success  center-block">Submit</button>

                    </fieldset>
                </form>


            </div>
        </div>


    </div>
    </div>
    </div>

    <form name="frmChkForm" id="frmChkForm">
        <input type="checkbox" name="chkcc9" id="group1">Check Me
        <input type="checkbox" name="chk9[120]" class="group1">
        <input type="checkbox" name="chk9[140]" class="group1">
        <input type="checkbox" name="chk9[150]" class="group1">
    </form>
    <script>

    </script>

@endsection

