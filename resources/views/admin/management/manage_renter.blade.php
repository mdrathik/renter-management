<script src="{{asset('public/admin/vendor/jquery/jquery.js')}}"></script>
<script src="{{asset('public/admin/custom/dataInsert.js')}}"></script>



@extends('layouts.master')
@section('content')
    <h1 class="section-title">Manage Your Renter - <strong
                style="color: forestgreen">{{$property_details->building_name}}</strong></h1>
    <h4 class="text-center">{{$property_details->building_address}} #{{$property_details->reference_id}}</h4>
    <div class="title-underline"></div>
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">

                <div class="panel panel-primary filterable">
                        <div class="pull-right">
                            <button class="btn btn-success btn-filter"><span class="glyphicon glyphicon-search"></span>Search</button>
                        </div>
                    <table class="table">
                        <thead>
                        <tr class="filters">
                            <th><input type="text" class="form-control" placeholder="Username" disabled></th>
                            <th><input type="text" class="form-control" placeholder="Type" disabled></th>
                            <th><input type="text" class="form-control" placeholder="Role" disabled></th>
                            <th><input type="text" class="form-control" placeholder="Status" disabled></th>
                            <th><input type="text" class="form-control" placeholder="Start-Date" disabled></th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                    @foreach($renter_details as $key)
                        <tr >


                            <td>{{$key->username}}</td>
                            <td>{{$key->type}}</td>
                            <td>{{$key->role}}</td>
                            @if($key->publication_status==true)
                            <td>Published</td>
                             @else
                                <td>Unpublished</td>
                            @endif
                            <td>{{$key->date}}</td>
                            <form method="POST" action="{{url('owner/renter-dashboard')}}">
                                {{csrf_field()}}
                                <input hidden name="reference_id" value="{{$property_details->reference_id}}">
                                <input hidden name="renter_reference" value="{{$key->renter_reference}}">
                                <td>
                                <button title="DashBoard"  class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-dashboard"></span></button>
                                <button title="Published" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-arrow-up"></span></button>
                                &nbsp;<br><button title="Delete" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
                                <button title="Delete" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-envelope"></span></button>
                            </td>
                            </form>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>



    </div>
    </div>
    </div>
@endsection
