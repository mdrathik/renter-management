<script src="{{asset('public/admin/vendor/jquery/jquery.js')}}"></script>
<script src="{{asset('public/admin/custom/dataInsert.js')}}"></script>




@extends('layouts.master')
@section('content')
    <h1 class="section-title">Here you can Change</h1>
    <div class="title-underline"></div>
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-10">
                <div class="col-xs-3">
                    <div class="project project-default">
                        <div class="shape">
                            <div class="shape-text">
                                floor 1
                            </div>
                        </div>
                        <div class="project-content">
                            <h3 class="lead">
                                Project label
                            </h3>
                            <p>
                                And a little description.
                                <br> and so one
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-xs-3">
                    <div class="project project-success">
                        <div class="shape">
                            <div class="shape-text">
                                top
                            </div>
                        </div>
                        <div class="project-content">
                            <h3 class="lead">
                                Project label
                            </h3>
                            <p>
                                And a little description.
                                <br> and so one
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-xs-3">
                    <div class="project project-radius project-primary">
                        <div class="shape">
                            <div class="shape-text">
                                top
                            </div>
                        </div>
                        <div class="project-content">
                            <h3 class="lead">
                                Project label
                            </h3>
                            <p>
                                And a little description.
                                <br> and so one
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-xs-3">
                    <div class="project project-info">
                        <div class="shape">
                            <div class="shape-text">
                                top
                            </div>
                        </div>
                        <div class="project-content">
                            <h3 class="lead">
                                Project label
                            </h3>
                            <p>
                                And a little description.
                                <br> and so one
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-xs-3">
                    <div class="project project-radius project-warning">
                        <div class="shape">
                            <div class="shape-text">
                                top
                            </div>
                        </div>
                        <div class="project-content">
                            <h3 class="lead">
                                Project label
                            </h3>
                            <p>
                                And a little description.
                                <br> and so one
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-xs-3">
                    <div class="project project-radius project-danger">
                        <div class="shape">
                            <div class="shape-text">
                                top
                            </div>
                        </div>
                        <div class="project-content">
                            <h3 class="lead">
                                Project label
                            </h3>
                            <p>
                                And a little description.
                                <br> and so one
                            </p>
                        </div>
                    </div>
                    </div>

                    <div class="col-xs-3">
                        <div class="project project-radius project-danger">
                            <div class="shape">
                                <div class="shape-text">
                                    top
                                </div>
                            </div>
                            <div class="project-content">
                                <h3 class="lead">
                                    Project label
                                </h3>
                                <p>
                                    And a little description.
                                    <br> and so one
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-3">
                        <div class="project project-radius project-danger">
                            <div class="shape">
                                <div class="shape-text">
                                    top
                                </div>
                            </div>
                            <div class="project-content">
                                <h3 class="lead">
                                    Project label
                                </h3>
                                <p>
                                    And a little description.
                                    <br> and so one
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
