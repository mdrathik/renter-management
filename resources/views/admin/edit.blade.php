@extends('layouts.master')
@section('content')
    <?php
    function generateRandomString($length = 10) {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    function generateRandomStringPass($length =8) {
        $characters = '1234567890#%@abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    ?>

    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    {{--    start input--}}
                    <div class="panel">
                        <div class="panel-heading">
                            <h1 class="panel-title text-center">Manage Your Renter Details [ {{$property_ById[0]->reference_id }} ]</h1>
                        </div>
                        <div class="panel-body">
                            <form method="post" action="{{url('owner/history')}}">
                                {{csrf_field()}}
                                <input name="reference_id" type="hidden" value="{{$property_ById[0]->reference_id }}">
                                <label>Select Floor</label>
                                <select name="floor"  class="form-control input-lg">
                                    <?php $count=0;
                                    foreach($property_ById as $key){?>
                                        <option value="<?php  echo $count++ ?>"><?php echo $count?></option>
                                   <?php } ?>
                                </select>
                                <br>
                                <label>Select Your  Flat</label>
                                <select name="flat" class="form-control input-lg">
                                    <option value="1">Left</option>
                                    <option value="2">Middle</option>
                                    <option value="3">Right</option>
                                </select>
                                <br>
                                <label>Full Name</label>
                                <input type="text" name="fullname"  class="form-control" placeholder="">
                                <br>
                                <label>Email</label>
                                <input type="email" name="email"  class="form-control" placeholder="">
                                <br>
                                <label>UserName</label>
                                <input type="name" name="username" value="<?php echo generateRandomString()?>" class="form-control" placeholder="">
                                <br>
                                <label>Password</label>
                                <input type="name" name="password" value="<?php echo generateRandomStringPass()?>" class="form-control" placeholder="">
                                <br>
                                <label>Mobile</label>
                                <input type="number" name="mobile"  class="form-control" placeholder="">
                                <br>
                                <label>Address</label>
                                    <textarea type="text" name="address" rows="5"  class="form-control" placeholder=""></textarea>
                                <br>
                                <button type="submit" class="btn btn-block btn-primary">Submit</button>
                            </form>
                        </div>
                        {{--  end input--}}
                    </div>
                </div>
            </div>


        </div>
    </div>

@endsection