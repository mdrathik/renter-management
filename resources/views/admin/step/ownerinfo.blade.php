<script src="{{asset('public/admin/vendor/jquery/jquery.js')}}"></script>
<script src="{{asset('public/admin/custom/dataInsert.js')}}"></script>
@extends('layouts.master')
@section('content')
    <div class="main-content">
        <div class="container-fluid">
            <!-- OVERVIEW -->

            {{-----------------------------Notification-----------------------------------------}}
            <?php $notification=Session::get('notification');?>
            @if(isset($notification))
                <div  class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo Session::get('notification');
                Session::put('notification',null);
                ?>
            </div>
            @endif


            <?php $notification=Session::get('registration');?>
            @if(isset($notification))
                <div  class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <?php echo Session::get('registration');
                    Session::put('registration',null);
                    ?>
                </div>
            @endif
            {{-----------------------------Notification end-----------------------------------------}}





            {{------------------------Owner personal Information--------------------------------------------------------------}}
                @if(isset($step)&& ($step==1))
                    <div class="col-md-7 col-lg-offset-2 form-horizontal">
                        <h3 class="text-center">Enter Your Personal Information <span
                                    class="glyphicon glyphicon-user"></span></h3>
                        <br>
                        <form class="form-horizontal" method="post" action={{url('owner/personal-info')}}>
                            {{csrf_field()}}


                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Full Name</label>
                                <div class="col-sm-9">
                                    <input type="text" required name="fname" class="form-control" id="inputEmail3"
                                           placeholder="ex: Md Solaiman Hossain">
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Email Address</label>
                                <div class="col-sm-9">
                                    <input type="email" required name="email" class="form-control" id="inputEmail3"
                                           placeholder="ex : md.rathik@gmail.com">
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Mobile</label>
                                <div class="col-sm-9">
                                    <input type="number" required name="mobile" class="form-control" id="inputEmail3"
                                           placeholder="ex : 01676544310">
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Present Address</label>
                                <div class="col-sm-9">
                                    <input type="text" required name="present_address" class="form-control" id="inputEmail3"
                                           placeholder="ex : Sukrabad 40/2, Dhaka-1207">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Permanent Address</label>
                                <div class="col-sm-9">
                                    <input type="text" required name="permanent_address" class="form-control"
                                           id="inputEmail3" placeholder="ex: Sukrabad 40/2, Dhaka-1207">
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-10">
                                    <button type="submit" class="btn btn-primary">Next</button>
                                </div>
                            </div>
                        </form>
                    </div>
                @endif
                {{------------------------personal Information end--------------------------------------------------------------}}






                {{------------------------Building Information--------------------------------------------------------------}}
                @if(isset($step)&& ($step==2))
                    <div class="col-md-7 col-lg-offset-2 form-horizontal">
                        <h3 class="text-center">Create Your Property <span class="glyphicon glyphicon-home"></span></h3>
                        <br>
                        <form method="post" action={{url("owner/building-information")}} class=&quot;form-horizontal&quot;>
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Reference Number</label>
                                <div class="col-sm-9">
                                    <input type="text" readonly name="reference_id"
                                           value="<?php echo rand(1000000, 9999999); ?>" class="form-control"
                                           placeholder="<?php echo rand(10000, 100000000); ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Building Name</label>
                                <div class="col-sm-9">
                                    <input required type="text" name="bname" class="form-control" id="inputEmail3"
                                           placeholder="Building  Name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Building Address</label>
                                <div class="col-sm-9">
                                    <input type="text" required name="building_address" class="form-control"
                                           id="inputEmail3" placeholder="Building  Address">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Floor</label>
                                <div class="col-sm-9">
                                    <input type="number" required name="floor" class="form-control" id="inputEmail3"
                                           placeholder="Floor">
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-10">
                                    <button type="submit" class="btn btn-primary">Next</button>
                                </div>
                            </div>
                        </form>
                    </div>
                @endif
                {{------------------------End  Building Information--------------------------------------------------------------}}





                {{------------------------create property information--------------------------------------------------------------}}
                @if(isset($step)&& ($step==3))
                    <div class="col-md-7 col-lg-offset-2 form-horizontal">

                        <h3 class="text-center">Create Your Property Details For Floor
                            no.{{($total_flat->floor+1)-($total_flat->competence)}}</h3>
                        <br>
                        <form class="form-horizontal" method="post" action={{url('owner/flat-information')}} >

                            {{csrf_field()}}
                            <input type="hidden" name="competence" value={{$total_flat->competence}}>
                            <input type="hidden" name="reference_id" value={{$total_flat->reference_id}}>
                            <input type="hidden" name="floor" value={{($total_flat->floor+1)-($total_flat->competence)}}>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 control-label">How Many Flat on
                                    Floor {{($total_flat->floor+1)-($total_flat->competence)}}</label>
                                <div class="col-sm-8">
                                    <input id="flatcount" type="number" class="form-control" placeholder="ex: 3">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3"

                                       class="col-sm-4 control-label"></label>
                                <div class="col-sm-8" id="row">

                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-10">
                                    <button type="submit" class="btn btn-primary">Next</button>
                                </div>
                            </div>
                        </form>
                    </div>
                @endif
            {{------------------------ end property information--------------------------------------------------------------}}




                {{-------------------------start man poweer----------------------------------------------------------------------}}
                @if(isset($step)&& ($step==4))
                    <h3 class="text-center">Enter Your Worker/Manager Data<span
                                class="glyphicon glyphicon-user"></span></h3>
                    <br>

                    <form class="form-horizontal" method="post" action={{url('owner/man-power')}}>
                        {{csrf_field()}}
                        <input type="hidden" name="reference_id" value={{$total_flat->reference_id}}>

                        <div class="input_fields_wrap">

                            <div style="display: flex">
                                <input name="post[]" required type="text" class="form-control"
                                       placeholder="ex: Manager/Post"> &nbsp;&nbsp;
                                <input name="salary[]" required type="number" class="form-control"
                                       placeholder="ex: 3000 BDT"> &nbsp;&nbsp;
                                <input type="text" required name="name[]" type="number" class="form-control"
                                       placeholder="ex: Md Example uddin"> &nbsp;&nbsp;
                                {{--  <input type="text" name="mytext[]">--}}
                                <button class="add_field_button btn btn-success"><span
                                            class="glyphicon glyphicon-plus-sign"></span></button>
                            </div>
                            <br>
                        </div>

                        <button type="submit" class="btn btn-primary center-block">Submit Data</button>

                    </form>
                    `     @endif
            {{-------------------------start man poweer end----------------------------------------------------------------------}}





            {{----------------------------------------------------------------------------------------------------}}


            {{-----------------------------------------------------------------------------------------------------}}
            <!-- END OVERVIEW -->


            </div>
            <br>
            <div class="container">
                <div class="row">
                    <div class="col-md-11">

                        <h3 class="progress-title">Progressing</h3>
                        <div class="progress green">
                            <div class="progress-bar progress-bar-striped active"
                                 style="width:{{$step*20}}%; background:#ff4b7d;">
                                <div class="progress-value">{{$step*20}}%</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $('.progress .progress-bar').css("width",
                    function () {
                        return $(this).attr("aria-valuenow") + "%";
                    }
                )
            });
        </script>

        <script>
            window.setTimeout(function() {
                $(".alert").fadeTo(500, 0).slideUp(500, function(){
                    $(this).remove();
                });
            }, 5000);
        </script>
    @endsection