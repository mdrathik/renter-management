@extends('layouts.master')
@section('content')
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    {{--    start input--}}
                    <div class="panel">
                        <div class="panel-heading">
                            <h1 class="panel-title text-center">Input Your Data</h1>
                        </div>
                        <div class="panel-body">
                            <form method="post" action="">
                                <input type="text" disabled name="name" class="form-control" placeholder="{{Session::get('name')}}">
                                <br>
                                <input type="text"  disabled name="reference_id" value="<?php echo rand(1000000, 9999999); ?>" class="form-control" placeholder="<?php echo rand(10000, 100000000); ?>">
                                <br>
                                <input type="email" name="email" class="form-control" placeholder="md@gmail.com">
                                <br>
                                <input type="number" name="floor" class="form-control" placeholder="How Many Floor have">
                                <br>
                                <input type="number" name="floor" class="form-control" placeholder="How Many flat have in each floor">
                                <br>
                                <textarea name="address" class="form-control" placeholder="Address" rows="2"></textarea>
                                <br>
                                <textarea name="descriptions" class="form-control" placeholder="Your Property Descriptions" rows="4"></textarea>
                                <br>

                                <label class="fancy-checkbox">
                                    <input disabled type="checkbox">
                                    <span>Generate Random ID for Every Flat</span>
                                </label>
                                <br>

                                <button type="submit" class="btn btn-block btn-primary">Submit</button>

                            </form>
                        </div>
                        {{--  end input--}}
                    </div>
                </div>
            </div>


        </div>
    </div>

@endsection