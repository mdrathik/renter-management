<script src="{{asset('public/admin/vendor/jquery/jquery.js')}}"></script>
<script src="{{asset('public/admin/custom/dataInsert.js')}}"></script>
@extends('layouts.master')
@section('content')
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-5">
                {{--    start input--}}
            <div class="panel">
                <div  class="panel-heading">
                    <h1 id="alert" class="panel-title text-center"></h1>
                </div>
                <div class="panel-body">
                    <form id="ownerForm">
                        {{csrf_field()}}
                        <input  type="hidden"   name="owner_id"  value="{{Session::get('id')}}" class="form-control" placeholder="{{Session::get('id')}}">
                    <input type="text" readonly name="name"  value="{{Session::get('name')}}" class="form-control" placeholder="{{Session::get('name')}}">
                    <br>
                        <input type="text" readonly  name="reference_id" value="<?php echo rand(1000000, 9999999); ?>" class="form-control" placeholder="<?php echo rand(10000, 100000000); ?>">
                        <br>
                    <input type="email" name="email" class="form-control" placeholder="md@gmail.com">
                    <br>
                    <input id="flatcount" type="number" name="floor" class="form-control" placeholder="How Many Floor have">
                    <br>
                        <div id="row">

                        </div>
                    <br>
                        <textarea name="address" class="form-control" placeholder="Address" rows="2"></textarea>
                        <br>
                    <textarea name="descriptions" class="form-control" placeholder="Your Property Descriptions" rows="4"></textarea>
                    <br>

                    <label class="fancy-checkbox">
                        <input disabled type="checkbox">
                        <span>Generate Random ID for Every Flat</span>
                    </label>
                    <br>

                        <button type="button" id=ownerData class="btn btn-block btn-primary">Submit</button>

                    </form>
                </div>
                {{--  end input--}}


            </div>
                    </div>
                <!-- BORDERED TABLE -->
                    <div class="col-md-7">
        {{Session::get('msg')}}
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Manage Your Properties</h3>
                            </div>
                            <div class="panel-body">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>R_ID</th>
                                        <th>Address</th>
                                        <th>Manage User</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                  <?php  $property = DB::table('property_details')->where('name',Session::get('name'))->get() ?>
                                  @foreach($property as $key)
                                    <tr>
                                        <td>{{$key->reference_id}}</td>
                                        <td>{{$key->address}}</td>
                                        <th>
                                            <form style="display: inline" method="post" action="{{url('owner/edit')}}">
                                                {{csrf_field()}}
                                                <input  type="number" name="reference_id" hidden value="{{$key->reference_id}}">
                                            <button type="submit" title="Create User" class="btn btn-xs btn-success"><span class="fa fa-sign-out"></span></button>
                                            </form>
                                            <form method="post" action="{{url('owner/delete')}}" style="display:inline-flex">
                                                {{csrf_field()}}
                                                <input  type="number" name="reference_id" hidden value="{{$key->reference_id}}">
                                                <button  type="submit" title="Delete" class="btn btn-xs btn-danger"><span class="fa fa-trash"></span></button>
                                            </form>
                                        </th>
                                    </tr>
                                      @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                <!-- END BORDERED TABLE -->
                </div>
        </div>
    </div>

@endsection