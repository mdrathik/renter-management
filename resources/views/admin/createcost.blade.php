<script src="{{asset('public/admin/vendor/jquery/jquery.js')}}"></script>
<script src="{{asset('public/admin/custom/createcost.js')}}"></script>
@extends('layouts.master')
@section('content')
<?php
function generateRandomString($length =7) {
    $characters = '@#abcdefghuvwxyz1234';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}?>

    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="invoice">
                        Make Voucher for {{$renter->user_fullname}}
                    </div>
                    <div style="width:100%; padding:0px 0px;border-bottom:1px solid rgba(0,0,0,0.2);float:left;">
                        <div style="width:30%; float:left;padding:5px;">
                            To,
                            <span style="font-size:14px;float:left;width:100%;">{{$renter->user_fullname}}</span>
                            <span style="font-size:14px;float:left;width:100%;">{{$renter->username}}</span>
                            <span style="font-size:14px;float:left;width:100%;">{{$renter->email}}</span>
                            <span style="font-size:14px;float:left;width:100%;">{{$renter->mobile}}</span>
                            <span style="font-size:14px;float:left;width:100%;">{{$renter->floor}} Floor,
                                @if($renter->flat==1)Left Side</span>
                                @elseif($renter->flat==2)Middle Side</span>
                                @elseif($renter->flat==3)Right Side</span>
                                    @endif
                        </div>

                        <div style="width:40%; float:right;padding-right: 5px">
                            <span style="float:right; text-align:right;">From,</span><br>
                            <span style="font-size:14px;float:right; text-align:right;">Owner : {{$owner->name}}</span><br>
                <span style="font-size:14px;float:right; text-align:right;">
                    <b>Date : </b><?php $date = new DateTime();
                    $date->setTimeZone(new DateTimeZone("Asia/Dhaka"));
                    echo $get_datetime = $date->format('d.m.Y');?>
                </span>
                             <span style="font-size:14px;float:right;  text-align:right;">{{$owner->email}}</span>
                        </div>
                    </div>


                    <div style="width:100%; padding:0px; float:left;">
                        <div style="width:100%;float:left;background:#efefef;">
                <span style="float:left; text-align:left;padding:10px;width:50%;color:#000;font-weight:600;">Decription</span>
             <span style="font-weight:600;float:left;padding:10px ;width:40%;color:#000;text-align:right;">Amount</span>
                        </div>



                        <form class="form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="reference_id" value="{{$renter->reference_id}}">
                            <input type="hidden" name="invoice_id" value="{{generateRandomString()}}">
                            <input type="hidden" name="owner" value="{{$owner->name}}">
                            <input type="hidden" name="username" value="{{$renter->username}}">
                            <input type="hidden" name="floor" value="{{$renter->floor}}"}}>
                            <input type="hidden" name="flat" value="{{$renter->flat}}"}}>



                        <div style="width:100%;float:left;">
                             <span class="invoice_text">House Rent</span>
                            <input type="number" class="invoice_text_price num1 key"   name="house_rent">
                        </div>


                <div style="width:100%;float:left;">
                            <span class="invoice_text">Gas Bill</span>
                            <input type="number" class="invoice_text_price num2 key"  name="gas_bill">
                </div>

                            <div style="width:100%;float:left;">
                                <span class="invoice_text">Water Bill</span>
                                <input type="number" class="invoice_text_price num3 key"   name="water_bill">
                            </div>

                            <div style="width:100%;float:left;">
                                <span class="invoice_text">Current Bill</span>
                                <input type="number" class="invoice_text_price num4 key" name="current_bill">
                            </div>

                            <div style="width:100%;float:left;">
                                <span class="invoice_text">Maintenance Charge</span>
                                <input type="number" class="invoice_text_price num5 key"   name="maintainance_charge">
                            </div>
                            <div style="width:100%;float:left;">
                                <span class="invoice_text">Generator Charge</span>
                                <input type="number" class="invoice_text_price num6 key"    name="generator_bill">
                            </div>

                            <div style="width:100%;float:left;">
                                <span class="invoice_text">Extra Charge</span>
                                <input type="number" type="number" class="invoice_text_price num7 key"  name="extra_charge">
                            </div>

                            <div style="width:100%;float:left;">
                                <span class="invoice_text">Comment</span>
                               <textarea  class="invoice_text_commet" rows="3" name="comment">No Comment</textarea>
                            </div>

                        <div style="width:100%;float:left; background:#fff;">
             <span class="sum" style="font-weight:600;float:right;padding:10px 0px;margin-right:10px;color:#666;text-align:center;">
               Total : 0 BDT
            </span>
                        </div>

                            <button type="button" disabled id="submit" class="btn btn-block btn-success">Send Invoice</button>
                        </form>

                    </div>
                </div>
                <div class="col-md-6">

                    <div class="invoice_waiting">
                        Pending List
                    </div>
                    <div class="span5">
                        <table  class="table table-striped table-condensed">
                            <thead>
                            <tr>
                                <th>Invoice Id</th>
                                <th>Date</th>
                                <th>Total</th>
                                <th>Floor/Flat</th>
                                <th>Status</th>
                                <th class="text-center">Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            @if($pending==null)
                                <tr>
                                   <td><h3 c>No pending Request</h3></td>
                                </tr>
                                @else
                            <tr>
                            <td>{{$pending->invoice_id}}</td>
                                <td>{{$pending->created_at}}</td>
                                <td>{{$pending->house_rent+$pending->current_bill+$pending->gas_bill+$pending->water_bill+$pending->maintainance_charge+$pending->generator_bill+$pending->extra_charge}} BDT</td>
                            <td>{{$pending->floor}}/{{$pending->flat}}</td>
                                @if($pending->status==1)
                            <td><i title="wating for renter response" class='glyphicon glyphicon-refresh gly-spin'></i></td>
                                    <td>Wating For Renter response</td>
                               @elseif($pending->status==2)
                                    <td><i title="Renter Paid successfully" class='glyphicon glyphicon-ok'></i></td>
                                    <td>
                                        <form style="display: inline-flex" method="POST" action="{{url('owner/rentcost')}}">
                                            {{csrf_field()}}
                                            <input type="hidden" name="reference_id" value="{{$renter->reference_id}}">
                                            <input type="hidden" name="username" value="{{$renter->username}}">
                                            <input type="hidden" name="invoice_id" value="{{$pending->invoice_id}}">

                                            <button name="button" value="accept"  title='accept' class='btn-xm btn-primary'><span class='glyphicon glyphicon-check'></span></button>
                                            <button name="button" value="decline" title='decline' class='btn-xm btn-danger'><span class='glyphicon glyphicon-remove'></span></button></td>
                                    </form>
                            </tr>

                                @endif

                                @endif
                            </tbody>
                        </table>



                            <h4 class="text-danger" style="float: left">This Year History</h4>
                            <div  class="form-group pull-right">
                                <input type="text" class="search form-control" placeholder="What you looking for?">
                            </div>
                            <span class="counter pull-right"></span>
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                <tr>
                                    <th>invoice</th>
                                    <th>House</th>
                                    <th>Gas</th>
                                    <th>Water</th>
                                    <th>Current</th>
                                    <th>Maint.</th>
                                    <th>Extra</th>
                                    <th>Total</th>
                                    <th>Date</th>
                                </tr>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($successfully_paid as $key)
                                    <tr>
                                        <td>{{$key->invoice_id}}</td>
                                        <td>{{$key->house_rent}}</td>
                                        <td>{{$key->gas_bill}}</td>
                                        <td>{{$key->water_bill}}</td>
                                        <td>{{$key->current_bill}}</td>
                                        <td>{{$key->maintainance_charge}}</td>
                                        <td>{{$key->extra_charge}}</td>
                                        <td>{{$key->house_rent+$key->current_bill+$key->gas_bill+$key->water_bill+$key->maintainance_charge+$key->generator_bill+$key->extra_charge}} BDT</td>

                                        <td>{{date("F",strtotime($key->created_at))}}</td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>



                    </div>
                </div>

            </div>
        </div>

    </div>
<script>
    $(document).ready(function() {
        $(".search").keyup(function () {
            var searchTerm = $(".search").val();
            var listItem = $('.results tbody').children('tr');
            var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

            $.extend($.expr[':'], {'containsi': function(elem, i, match, array){
                return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
            }
            });

            $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
                $(this).attr('visible','false');
            });

            $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
                $(this).attr('visible','true');
            });

            var jobCount = $('.results tbody tr[visible="true"]').length;
            $('.counter').text(jobCount + ' item');

            if(jobCount == '0') {$('.no-result').show();}
            else {$('.no-result').hide();}
        });
    });

</script>
@endsection
