@extends('layouts.rentmaster')
@section('content')

    <div class="main-content">
        <div class="container-fluid">
            <!-- OVERVIEW -->
            <h3 class="text-center">{{Session::get('msg')}}</h3>
            @if($HouseCostingByUsername==null)
                <h1>You Have No Pending Invoice</h1>
            @else
            <div class="col-md-6">
                <div style="width:100%; float:left; background:#1ca8dd; color:#fff; font-size:30px; text-align:center;">
                    Invoice
                </div>
                <div style="width:100%; padding:0px 0px;border-bottom:1px solid rgba(0,0,0,0.2);float:left;">
                    <div style="width:30%; float:left;padding:10px;">

                        From,
                        <span style="font-size:14px;float:left;width:100%;">{{$renter->user_fullname}}</span>
                        <span style="font-size:14px;float:left;width:100%;">{{$renter->username}}</span>
                        <span style="font-size:14px;float:left;width:100%;">{{$renter->email}}</span>
                        <span style="font-size:14px;float:left;width:100%;">{{$renter->mobile}}</span>
                            <span style="font-size:14px;float:left;width:100%;">{{$renter->floor}} Floor,
                                @if($renter->flat==1)Left Side</span>
                        @elseif($renter->flat==2)Middle Side</span>
                        @elseif($renter->flat==3)Right Side</span>
                        @endif
                    </div>

                    <div style="width:40%; float:right;padding:">
            <span style="font-size:14px;float:right; text-align:right;">
                    <b>Date : </b><?php $date = new DateTime();
                $date->setTimeZone(new DateTimeZone("Asia/Dhaka"));
                echo $get_datetime = $date->format('d.m.Y');?>
                </span>
			<span style="font-size:14px;float:right;  padding:10px; text-align:right;">
               <b>Invoice# : {{$HouseCostingByUsername->invoice_id}}</b>
            </span>
                    </div>
                </div>





                <div style="width:100%; padding:0px; float:left;">

                    <div style="width:100%;float:left;background:#efefef;">
            <span style="float:left; text-align:left;padding:10px;width:50%;color:#888;font-weight:600;">
            Decription
           </span>
         <span style="font-weight:600;float:left;padding:10px ;width:40%;color:#888;text-align:right;">
         Amount
        </span>
                    </div>

                    <div style="width:100%;float:left;">


                            <span class="invoice_text">House Rent</span>
                            <span class="invoice_text_normal">{{$HouseCostingByUsername->house_rent}}</span>
                        <span class="invoice_text">Gas Bill</span>
                        <span class="invoice_text_normal">{{$HouseCostingByUsername->gas_bill}}</span>
                        <span class="invoice_text">Water Bill</span>
                        <span class="invoice_text_normal">{{$HouseCostingByUsername->water_bill}}</span>
                        <span class="invoice_text">Current Bill</span>
                        <span class="invoice_text_normal">{{$HouseCostingByUsername->current_bill}}</span>
                        <span class="invoice_text">Maintainance_charge Bill</span>
                        <span class="invoice_text_normal">{{$HouseCostingByUsername->maintainance_charge}}</span>
                        <span class="invoice_text">Generator_bill Bill</span>
                        <span class="invoice_text_normal">{{$HouseCostingByUsername->generator_bill}}</span>
                        <span class="invoice_text">Extra Charge</span>
                        <span class="invoice_text_normal">{{$HouseCostingByUsername->extra_charge}}</span>
                    </div>


                    <div style="width:100%;float:left; background:#fff;">

         {{--<span style="font-weight:600;float:right;padding:10px 0px;width:40%;color:#666;text-align:center;">
           Total : 373 INR
        </span>--}}
                            <form method="post" action="{{url('renter/history')}}">
                                {{csrf_field()}}
                                <input type="hidden" name="reference_id" value="{{$HouseCostingByUsername->reference_id}}">
                                <input type="hidden" name="invoice_id" value="{{$HouseCostingByUsername->invoice_id}}">
                        <button type="submit" class="btn btn-success btn-block">Accept And Pay</button>
                            </form>
                    </div>

                </div>

                <!-- END OVERVIEW -->
        </div>
            @endif
            <div class="col-md-6"></div>
    </div>
    </div>
@endsection