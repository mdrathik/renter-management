<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
session_start();

class RenterController extends Controller
{
    public  function  Index(){
            $username=Session::get('username');
            if($username==null) {
            return Redirect::to('renter/renterlogin')->send();
        }
        else
        {
            return view('renter.home');
        }
    }

    public  function  CheckRenterLogin(Request $request){
        $username =$request->username;
        $password=$request->password;
        $renterlogin= DB::table('rent_details')->where('username',$username)
            ->Where('password',$password)
            ->first();
        if($renterlogin){
            Session::put('id',$renterlogin->id);
            Session::put('username', $renterlogin->username);
            return Redirect::to('renter/myhome');
        }
        else {
            Session::put('error_msg','Username and Password is Incorrect');
            return Redirect::to('renter/renterlogin');
        }
    }
}
