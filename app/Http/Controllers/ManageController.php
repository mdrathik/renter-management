<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Redirect;
session_start();

class ManageController extends Controller
{
    //Admin LoginCheck when user change url without session
    public function loginCheck(){
      $admin_id=Session::get('admin_id');
        $admin_username=Session::get('admin_name');
        if($admin_id==null){
            Session::put('error_msg','Please Submit Email and Password');
            return Redirect::to('owner/login')->send();
        }
    }


    //Check and give notification when user go others without complete widget
    public function WidgetCheck(){
        $admin_id=Session::get('admin_id');
        $step_id=DB::table('owner_step')->where('owner_id',$admin_id)
            ->where('major',true)->first();

        if($step_id->step_id!=5){
            Session::put('notification','You Must Need to complete this process then your can add more Property');
            return Redirect::to('owner/login')->send();
        }


    }


    //Show all property on "your properties" menu
    public function allProperty(){
        $this->loginCheck();
        $admin_id=Session::get('admin_id');
        $all_property=DB::table('owner_property_details')->where('owner_id',$admin_id)->get();
        return view('admin.allproperty',['all_property'=>$all_property]);
    }



    //Add more property when widget is completed


    public function addMoreProperty(){
        $step=2;
        $this->loginCheck();
        $this->WidgetCheck();
        return view('admin.more_property.more_property')->with('step',$step);

    }


    //Information for another building
    public function MorebuildingInformation(Request $request){
        $admin_id=Session::get('admin_id');
        $building_info=DB::table('owner_property_details')->insert([
                'building_name' => $request->bname,
                'building_address' => $request->building_address,
                'floor' => $request->floor,
                'competence' => $request->floor,
                'reference_id'=>$request->reference_id,
                'owner_id'=>$admin_id,
        ]);

        if($building_info==true){
            DB::table('owner_step')->insert([
                'owner_id'=>$admin_id,
                'step_id'=>3,
                'major'=>false,
                'reference_id'=>$request->reference_id
            ]);
            return $this->MoreWidgets($admin_id,$request->reference_id);
        }
        else
            return "false";
    }


    //Information for another building

    public function addMoreflat(Request $request)
    {
        $flat = '';
        $admin_id = Session::get('admin_id');
        foreach ($request->flat as $key) {
            $flat = $flat . $key . "~";
        }
        $newf = chop($flat, "~");
        $floorbyflat = DB::table('floorbyflat')->insert([
            [
                'reference_id' => $request->reference_id,
                'floor' => $request->floor,
                'flat' => $newf,
                'owner_id' => $admin_id,
            ]
        ]);

        if ($floorbyflat == true) {
            $complence = $request->competence;
            $building_info = DB::table('owner_property_details')
                ->where('owner_id', $admin_id)
                ->where('reference_id', $request->reference_id)
                ->update(['competence' => $complence - 1]);
        }

        $complence_id = DB::table('owner_property_details')->where('owner_id', $admin_id)
            ->where('reference_id', $request->reference_id)
            ->first();

        if ($complence_id->competence == 0) {
            DB::table('owner_step')
                ->where('owner_id', $admin_id)
                ->where('reference_id', $request->reference_id)
                ->where('major', false)
                ->update(['reference_id' => $request->reference_id, 'step_id' => 4]);

        }

        return $this->MoreWidgets($admin_id, $request->reference_id);
    }


    //manPower Information
    public function addMoreman(Request $request){
        $admin_id=Session::get('admin_id');
        $post='';
        $salary='';
        $name='';

        foreach($_POST["name"] as $key=>$value){
            $name=$_POST["name"][$key];
            $salary=$_POST["salary"][$key];
            $post=$_POST["post"][$key];
            $manpower=DB::table('owner_manpower_details')->insert([
                [
                    'post'=>$post,
                    'salary'=>$salary,
                    'name'=>$name,
                    'reference_id'=>$request->reference_id,
                    'owner_id'=>$admin_id,
                ]
            ]);
        }

        if($manpower==true){
            DB::table('owner_step')
                ->where('owner_id', $admin_id)
                ->where('reference_id', $request->reference_id)
                ->where('major', false)
                ->update(['reference_id' => $request->reference_id, 'step_id' => 5]);

        }
        return $this->MoreWidgets($admin_id, $request->reference_id);
    }

    //edit function to edit incompleted property
    public function EditMoreProperty($reference_id,$owner_id){
        $this->loginCheck();
        $this->WidgetCheck();
        return $this->MoreWidgets($owner_id,$reference_id);
    }


    //More widget for edit and add more property
    public function MoreWidgets($owner_id,$reference_id){

        $step_id=DB::table('owner_step')->where('owner_id',$owner_id)
            ->where('major',false)
            ->where('reference_id',$reference_id)
            ->first();
        $step=$step_id->step_id;

        $total_flat=DB::table('owner_property_details')->where('owner_id',$owner_id)
                                                        ->where('reference_id',$reference_id)
            ->first();
        if($step==5) {
            return redirect::to("owner/all-properties");
        }
        else {

            var_dump($step);

            return view('admin.more_property.more_property')->with('step', $step)->with('total_flat', $total_flat);

        }
    }






/*-----------------------------------------------------------------------------------------------------------------------*/

//Management Property where can delete edit and manage home
public function ManageProperty($reference_id,$owner_id){
    $this->loginCheck();
    return view('admin.manage_property',['reference_id'=>$reference_id,'owner_id'=>$owner_id]);
}

public function EditManageProperty($reference_id,$owner_id){
    return $reference_id;
}







    public  function AddProperty(){
        //$property = DB::table('property_details')->where('votes', '=', 100)->get();
        $this->loginCheck();
       return view('admin.addproperty');
    }



    public  function DeleteProperty(Request $request){
        $this->loginCheck();
        DB::table('property_details')->where('reference_id', '=',$request->reference_id )->delete();
        DB::table('rent_details')->where('reference_id', '=',$request->reference_id )->delete();
        DB::table('flat_details')->where('reference_id', '=',$request->reference_id )->delete();
        DB::table('floorbyflat')->where('reference_id', '=',$request->reference_id )->delete();
        //return $request->all();
        return redirect('owner/add')->with('msg','Successfully Deleted  this Reference -'.$request->reference_id);
        //return view('admin.addproperty');
    }


















}
