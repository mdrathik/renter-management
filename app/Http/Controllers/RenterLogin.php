<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
session_start();
class RenterLogin extends Controller
{
    public  function  Index(){
        $username=Session::get('username');
        if($username!=null)
        {
            return Redirect::to('renter/myhome')->send();
        }
        else {
            return view('login.renterlogin');
        }

    }

    public  function  RenterLogout(){
        Session::put('id',null);
        Session::put('username',null);
        Session::put('error_msg','Renter Successfully Logout');
        return Redirect::to('renter/renterlogin');
    }

}
