<?php

namespace App\Http\Controllers;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Http\Request;
use DB;
use Illuminate\Session\Middleware\StartSession;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
session_start();
class OwnerController extends Controller
{

    //New admin /owner user registration page
    public  function Registration (){
        $id=Session::get('admin_id');
        if($id!=null) {
            return $this->Widgets($id);
        }
        else
        {
            return view('reg.owner');
        }

    }

    //New admin /owner user registration page and post method to send data into database
    public function NewRegistration(Request $request){
        $name=$request->name;
        $email=$request->email;
        $password=$request->password;
        $options = [
            'cost' => 12,
        ];
        $hash=password_hash($password, PASSWORD_BCRYPT, $options);
        $reg=DB::table('users')->insert
        ([
            'name'=>$name,
            'email'=>$email,
            'password'=>$password,

        ]);

        $login= DB::table('users')->where('email',$email)
            ->Where ('password',$password)
            ->first();

        if($reg==true && $login==true){
                  Session::put('registration','Registration Successfully Completed, Now Complete this Process please ');
                  Session::put('admin_name',$login->name);
                  Session::put('admin_id',$login->id);
                  DB::table('owner_step')->insert([
                     'owner_id'=>$login->id,
                      'step_id'=>1,
                      'major'=>true
                  ]);
            return $this->Widgets($login->id);
        }
    }





    //home function for direct home address that mean admin index page
    public  function  Home(){
        $admin_id=Session::get('admin_id');
        $admin_username=Session::get('admin_id');
        if($admin_id==null)
        {
            Session::put('error_msg','Please Submit Email and Password');
            return Redirect::to('owner/login')->send();
        }
        else {
            return $this->Widgets($admin_id);

        }
    }


    //Mainly check user login here , when user want to login from login page
    public  function  CheckLogin(Request $request){
        $email=$request->email;
        $password=$request->password;



        $login= DB::table('users')->where('email',$email)
                                ->Where ('password',$password)
                                ->first();


        if($login){
            //Session::put('admin','admin');
            Session::put('admin_id',$login->id);
            Session::put('admin_name', $login->name);
            return $this->Widgets($login->id);
           //return Redirect::to('owner/home');
        }
        else {
             $result=Session::put('error_msg','Email and Password is Incorrect');
            //return "failed";
            return Redirect::to('owner/login')->send();
        }
    }





    //Widgets View for step one to last
    public function Widgets($owner_id){

        $step_id=DB::table('owner_step')->where('owner_id',$owner_id)
                                        ->where('major',true)
            ->first();
        $step=$step_id->step_id;

        $total_flat=DB::table('owner_property_details')->where('owner_id',$owner_id)
            ->first();
        if($step==5) {
            return view('admin.home');
        }
        else {
            return view('admin.step.ownerinfo')->with('step', $step)->with('total_flat', $total_flat);

        }
    }


    //manage widgets

    //insert personal Information
    public function personalInformation(Request $request){
        $admin_id=Session::get('admin_id');
        $user_info=DB::table('owner_info')->insert([
            ['full_name' => $request->fname,
             'email' => $request->email,
             'mobile' => $request->mobile,
             'present_address'=>$request->present_address,
             'permanent_address'=>$request->permanent_address,
             'owner_id'=>$admin_id,
            ]
        ]);
        if($user_info==true){
            DB::table('owner_step')
                ->where('owner_id', $admin_id)
                ->update(['step_id' => 2,'major'=>true]);
        }
        return redirect::to("owner/home");
    }

    //Insert Building Information
  public function buildingInformation(Request $request){
      $admin_id=Session::get('admin_id');
      $building_info=DB::table('owner_property_details')->insert([
              ['building_name' => $request->bname,
              'building_address' => $request->building_address,
              'floor' => $request->floor,
              'competence' => $request->floor,
              'reference_id'=>$request->reference_id,
              'owner_id'=>$admin_id,
          ]
      ]);
      if($building_info==true){
          DB::table('owner_step')
              ->where('owner_id', $admin_id)
              ->update(['reference_id' =>$request->reference_id,'step_id'=>3])
                ;
      }
      else
          return "false";
      return redirect::to("owner/home");
    // return $request->all();
  }




    //Insert Flat and Floor Information
    public function flatInformation(Request $request){
        $flat='';
        $admin_id=Session::get('admin_id');
        foreach($request->flat as $key){
           $flat=$flat.$key."~";
        }

         $newf=chop($flat,"~");
        $floorbyflat=DB::table('floorbyflat')->insert([
            [
                'reference_id'=>$request->reference_id,
                'floor'=>$request->floor,
                'flat'=>$newf,
                'owner_id'=>$admin_id,
            ]
        ]);

        if($floorbyflat==true){
            $complence=$request->competence;
            $building_info=DB::table('owner_property_details')
                            ->where('owner_id',$admin_id)
                            ->where('reference_id',$request->reference_id)
                            ->update(['competence'=>$complence-1]);
        }

        $complence_id=DB::table('owner_property_details')->where('owner_id',$admin_id)
            ->where('reference_id',$request->reference_id)
            ->first();

        if($complence_id->competence==0){
            DB::table('owner_step')
                ->where('owner_id', $admin_id)
                ->update(['reference_id' =>$request->reference_id,'step_id'=>4]);
        }
        return redirect::to("owner/home");
    }






    //manPower Information
    public function manPower(Request $request){
        $admin_id=Session::get('admin_id');
        $post='';
        $salary='';
        $name='';

        foreach($_POST["name"] as $key=>$value){
            $name=$_POST["name"][$key];
            $salary=$_POST["salary"][$key];
            $post=$_POST["post"][$key];
            $manpower=DB::table('owner_manpower_details')->insert([
                [
                    'post'=>$post,
                    'salary'=>$salary,
                    'name'=>$name,
                    'reference_id'=>$request->reference_id,
                    'owner_id'=>$admin_id,
                ]
            ]);
        }

        if($manpower==true){
            DB::table('owner_step')
                ->where('owner_id', $admin_id)
                ->update(['reference_id' =>$request->reference_id,'step_id'=>5]);

        }

        return redirect::to("owner/home");
    }





    //logout function
    public  function  logout(){
        Session::put('admin_id',null);
        Session::put('admin_name',null);
        Session::put('error_msg','Successfully Logout');
        return Redirect::to('owner/login');
    }

}
