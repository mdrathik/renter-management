<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Redirect;
use DB;

session_start();

class RenterManageController extends Controller
{
    public  function  RenterLoginCheck(){
        $username=Session::get('username');
        if($username==null)
        {
            Session::put('error_msg','Please Submit Username and Password');
            return Redirect::to('renter/myhome')->send();
        }

    }


            public function Index(){
                $this->RenterLoginCheck();
                $username=Session::get('username');
                $HouseCostingByUsername=DB::table('house_costing')
                    ->where('username',$username)
                    ->where('status',1)->first();

                if($HouseCostingByUsername==null){
                    return view('renter.history')->with('HouseCostingByUsername',$HouseCostingByUsername);
                }
                if($HouseCostingByUsername!=null){
                    $r_id=$HouseCostingByUsername->reference_id;
                    $renter=DB::table('flat_details')
                        ->where('username',$username)
                        ->where('reference_id',$r_id)->first();
                    //return var_dump($flatDetails);
                    return view('renter.history')->with('HouseCostingByUsername',$HouseCostingByUsername)->with('renter',$renter);
                }

            }

            public function AcceptPayment(Request $request){
                DB::table('house_costing')
                    ->where('reference_id', $request->reference_id)
                    ->where('invoice_id',$request->invoice_id)
                    ->update(['status' => 2]);
                return redirect('renter/history')->with('msg',"Payment Completed");
            }





}
