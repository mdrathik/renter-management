<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

;
use Illuminate\Support\Facades\DB;
use Session;
use Redirect;
use Carbon\Carbon;


class OwnertoRenterManagement extends ManageController
{
    public function AddRenterManageProperty($reference_id, $owner_id)
    {
        $r_id = decrypt($reference_id);
        $this->loginCheck();
        $property_details = DB::table('owner_property_details')->where('Reference_id', $r_id)
            ->where('owner_id', $owner_id)->first();
        return view("admin.management.add_renter", ['property_details' => $property_details]);

    }

    public function ShowFloorDatajs($floor, $reference_id)
    {


        $floorbyflat = DB::table('floorbyflat')->where('reference_id', $reference_id)
            ->where('floor', $floor)
            ->first();

        $flats = $floorbyflat->flat;
        $flat = explode("~", $flats);
        foreach ($flat as $row) {
            echo '<option value="' . $row . '">' . $row . '</option>';
        }
        //return var_dump($flat);

    }


    public function SaveRenterData(Request $request)
    {
        //  return $request->all();
        $renter_data = DB::table('renter_property_user')->insert([
            'username' => $request->username,
            'password' => $request->password,
            'renter_reference' => $request->renter_reference,
            'publication_status' => true,
            "role" => $request->role,
            "type" => $request->type,
            'reference_id' => $request->reference_id,
            'owner_id' => $request->owner_id,
            'date' => Carbon::now(),
        ]);

        if ($renter_data == true) {
            $i = 0;
            foreach ($request->floor as $key) {
                $i++;
                $flat = "";

                if (isset($request->flat[$i])) {
                    foreach ($request->flat[$i] as $key2 => $value) {
                        $flat = $flat . $value . "~";
                    }

                    $newf = chop($flat, "~");
                    DB::table('renter_property_details')->insert([
                        'floor' => $i,
                        'flat' => $newf,
                        'checked' => true,
                        'renter_reference' => $request->renter_reference,
                        'reference_id' => $request->reference_id,
                    ]);
                }
            }
        }
        return redirect('owner/manage_renter/' . encrypt($request->reference_id) . '/' . $request->owner_id);

    }


    //edit or management data renter
    public function EditRenterManageProperty($reference_id, $owner_id)
    {
        $r_id = decrypt($reference_id);
        $this->loginCheck();
        $property_details = DB::table('owner_property_details')->where('Reference_id', $r_id)
            ->where('owner_id', $owner_id)->first();

        $renter_details = DB::table('renter_property_user')->where('Reference_id', $r_id)
            ->where('owner_id', $owner_id)->get();
        return view('admin.management.manage_renter', ['property_details' => $property_details], ['renter_details' => $renter_details]);
    }


    public function DataHandling($id, $floor, $flat, $reference)
    {
        /*echo $floor."<br>";
        echo $flat."<br>";*/
        $newflat = DB::table('renter_property_details')->where('reference_id', $reference)
            ->where('floor', $floor)
            ->get();
        $i = 0;
        foreach ($newflat as $key) {
            $floorbyflat = explode("~", $newflat[$i]->flat);
            $i++;
            foreach ($floorbyflat as $key1) {
                if (strcmp($key1, $flat) == 0) {
                    echo $id;
                }
            }
        }
    }

    public function RenterDashboard(Request $request){
        $renter_reference= $request->renter_reference;
        $reference_id=$request->reference_id;

        $details=DB::table('renter_property_details')->where('renter_reference',$renter_reference)->where('reference_id',$reference_id)
                ->get();

        //return var_dump($details);
        return view('admin.management.renter_dash')->with('renter_reference',$renter_reference);
    }
}
