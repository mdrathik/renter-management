-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 14, 2017 at 05:54 PM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `miciapps_house_rent`
--

-- --------------------------------------------------------

--
-- Table structure for table `floorbyflat`
--

CREATE TABLE `floorbyflat` (
  `id` int(10) UNSIGNED NOT NULL,
  `reference_id` int(11) NOT NULL,
  `floor` int(11) NOT NULL,
  `flat` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_id` int(3) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `floorbyflat`
--

INSERT INTO `floorbyflat` (`id`, `reference_id`, `floor`, `flat`, `owner_id`, `created_at`, `updated_at`) VALUES
(1, 8913191, 1, 'a1~a2', 1, NULL, NULL),
(2, 8913191, 2, 'a1~a2', 1, NULL, NULL),
(3, 3331598, 1, 'A1~A2~A3~A4', 2, NULL, NULL),
(4, 3331598, 2, 'A1~A2', 2, NULL, NULL),
(5, 3331598, 3, 'A1', 2, NULL, NULL),
(6, 3331598, 4, 'A1~A2', 2, NULL, NULL),
(7, 3331598, 5, 'A1', 2, NULL, NULL),
(8, 6121807, 1, 'A1~A2', 2, NULL, NULL),
(9, 6121807, 2, 'A1', 2, NULL, NULL),
(10, 6121807, 3, 'A1~A2', 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(5, '2014_10_12_000000_create_users_table', 1),
(6, '2014_10_12_100000_create_password_resets_table', 1),
(7, '2017_05_15_161935_create_property_table', 1),
(12, '2017_05_15_162357_create_property_details_table', 2),
(13, '2017_05_15_201805_create_rent_details', 2),
(14, '2017_06_02_092248_floorbyflat', 2);

-- --------------------------------------------------------

--
-- Table structure for table `owner_info`
--

CREATE TABLE `owner_info` (
  `id` int(11) NOT NULL,
  `full_name` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `mobile` int(14) NOT NULL,
  `present_address` varchar(50) NOT NULL,
  `permanent_address` varchar(50) NOT NULL,
  `owner_id` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `owner_info`
--

INSERT INTO `owner_info` (`id`, `full_name`, `email`, `mobile`, `present_address`, `permanent_address`, `owner_id`) VALUES
(1, 'Beeplob Karmokar', 'xxx@gmail.com', 168788797, 'Beside north side mosque, Kharabar', 'Beside north side mosque, Kharabar', 1),
(2, 'আমি', 'mdrathik@gmail.com', 1676544310, 'Beside north side mosque, Kharabar', 'Beside north side mosque, Kharabar', 2);

-- --------------------------------------------------------

--
-- Table structure for table `owner_manpower_details`
--

CREATE TABLE `owner_manpower_details` (
  `id` int(11) NOT NULL,
  `post` varchar(15) NOT NULL,
  `salary` int(7) NOT NULL,
  `name` varchar(20) NOT NULL,
  `reference_id` int(7) DEFAULT NULL,
  `owner_id` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `owner_manpower_details`
--

INSERT INTO `owner_manpower_details` (`id`, `post`, `salary`, `name`, `reference_id`, `owner_id`) VALUES
(1, 'Manager', 1000, 'Khalek', 8913191, 1),
(2, 'care taker', 2000, 'dualal', 8913191, 1),
(3, 'care taker', 1000, 'dualal', 3331598, 2),
(4, 'Manager', 3333, 'Khalid Hasan', 3331598, 2),
(5, 'care taker', 1000, 'Saiful islam', 6121807, 2),
(6, 'Vua', 500, 'Khala', 6121807, 2);

-- --------------------------------------------------------

--
-- Table structure for table `owner_property_details`
--

CREATE TABLE `owner_property_details` (
  `id` int(11) NOT NULL,
  `building_name` varchar(20) NOT NULL,
  `building_address` varchar(50) NOT NULL,
  `floor` int(2) NOT NULL,
  `competence` int(2) NOT NULL,
  `owner_id` int(2) NOT NULL,
  `reference_id` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `owner_property_details`
--

INSERT INTO `owner_property_details` (`id`, `building_name`, `building_address`, `floor`, `competence`, `owner_id`, `reference_id`) VALUES
(1, 'Ghatail Home', 'ShantiNagor', 2, 0, 1, 8913191),
(2, 'AngryBird Home', 'Beside north side mosque, Kharabar', 5, 0, 2, 3331598),
(3, 'Bazaru', 'Madhupur, Tangail', 3, 0, 2, 6121807);

-- --------------------------------------------------------

--
-- Table structure for table `owner_step`
--

CREATE TABLE `owner_step` (
  `id` int(11) NOT NULL,
  `step_id` int(11) DEFAULT NULL,
  `owner_id` int(11) NOT NULL,
  `reference_id` int(7) DEFAULT NULL,
  `major` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `owner_step`
--

INSERT INTO `owner_step` (`id`, `step_id`, `owner_id`, `reference_id`, `major`) VALUES
(1, 5, 1, 8913191, 1),
(2, 5, 2, 3331598, 1),
(3, 5, 2, 6121807, 0);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `renter_info`
--

CREATE TABLE `renter_info` (
  `id` int(11) NOT NULL,
  `full_name` int(11) NOT NULL,
  `mobile` int(11) NOT NULL,
  `email` int(11) NOT NULL,
  `renter_reference` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `renter_property_details`
--

CREATE TABLE `renter_property_details` (
  `id` int(11) NOT NULL,
  `floor` int(2) NOT NULL,
  `flat` varchar(50) NOT NULL,
  `checked` tinyint(1) NOT NULL,
  `renter_reference` int(5) NOT NULL,
  `reference_id` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `renter_property_details`
--

INSERT INTO `renter_property_details` (`id`, `floor`, `flat`, `checked`, `renter_reference`, `reference_id`) VALUES
(1, 1, 'A1~A2~A3', 1, 41272, '3331598'),
(2, 3, 'A1', 1, 41272, '3331598'),
(3, 5, 'A1', 1, 41272, '3331598'),
(4, 1, 'A1', 1, 49130, '6121807');

-- --------------------------------------------------------

--
-- Table structure for table `renter_property_user`
--

CREATE TABLE `renter_property_user` (
  `id` int(11) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(10) NOT NULL,
  `renter_reference` varchar(5) NOT NULL,
  `publication_status` tinyint(1) NOT NULL,
  `role` varchar(15) NOT NULL,
  `type` varchar(15) NOT NULL,
  `reference_id` varchar(7) NOT NULL,
  `owner_id` int(3) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `renter_property_user`
--

INSERT INTO `renter_property_user` (`id`, `username`, `password`, `renter_reference`, `publication_status`, `role`, `type`, `reference_id`, `owner_id`, `date`) VALUES
(1, 'gtteQcx$vJ', 'lRdyocApe3', '41272', 1, 'renter', 'single', '3331598', 2, '2017-07-13'),
(2, 'KjXnf7p6o3', 'xdoMWTAvVy', '49130', 1, 'renter', 'single', '6121807', 2, '2017-07-13');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Beeplob', 'beeplob@gmail.com', 'beeplob', NULL, NULL, NULL),
(2, 'admin', 'admin@admin.com', 'admin', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `floorbyflat`
--
ALTER TABLE `floorbyflat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owner_info`
--
ALTER TABLE `owner_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owner_manpower_details`
--
ALTER TABLE `owner_manpower_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owner_property_details`
--
ALTER TABLE `owner_property_details`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reference_id` (`reference_id`);

--
-- Indexes for table `owner_step`
--
ALTER TABLE `owner_step`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `renter_info`
--
ALTER TABLE `renter_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `renter_property_details`
--
ALTER TABLE `renter_property_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `renter_property_user`
--
ALTER TABLE `renter_property_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `renter_reference` (`renter_reference`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `floorbyflat`
--
ALTER TABLE `floorbyflat`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `owner_info`
--
ALTER TABLE `owner_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `owner_manpower_details`
--
ALTER TABLE `owner_manpower_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `owner_property_details`
--
ALTER TABLE `owner_property_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `owner_step`
--
ALTER TABLE `owner_step`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `renter_info`
--
ALTER TABLE `renter_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `renter_property_details`
--
ALTER TABLE `renter_property_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `renter_property_user`
--
ALTER TABLE `renter_property_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
